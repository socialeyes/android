package nguonchhay.reasei.socialeyes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import nguonchhay.reasei.socialeyes.database.ReporterContract;
import nguonchhay.reasei.socialeyes.database.SocialEyeDbHelper;
import nguonchhay.reasei.socialeyes.model.Reporter;
import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.LoginClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends Activity {

    private APIService apiService = new APIService();
    private boolean isAuth;
    private SocialEyeDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        dbHelper = new SocialEyeDbHelper(this);
        checkAuthenticate();
    }

    /**
     * @return boolean
     */
    public void checkAuthenticate() {
        Reporter reporter = ReporterContract.getReporter(dbHelper.getReadableDatabase());
        String accessToken = reporter.getRememberToken();
        isAuth = false;
        LoginClient loginClient = apiService.createClientWithJson(LoginClient.class, APIService.getAccessTokenForRequest(accessToken));
        loginClient.checkUserSession().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == HttpUtility.STATUS_CODE_OK) {
                    isAuth = true;
                }
                startAnotherActivity();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(SplashActivity.this, "Cannot make an request to server.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void startAnotherActivity() {
        Intent intent = null;
        if (isAuth) {
            intent = new Intent(SplashActivity.this, MainActivity.class);
        } else {
            intent = new Intent(SplashActivity.this, LoginActivity.class);
        }

        startActivity(intent);
        finish();
    }
}
