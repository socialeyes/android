package nguonchhay.reasei.socialeyes.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nguonchhay.reasei.socialeyes.R;
import nguonchhay.reasei.socialeyes.model.ReportStatus;
import nguonchhay.reasei.socialeyes.utility.DownLoadImageUtility;

public class ReportDetailAdapter extends BaseAdapter {

    private Context context;
    private List<String> id;
    private List<String> image;
    private List<String> name;
    private List<String> status;
    private List<String> contactPhone;
    private List<String> email;

    public ReportDetailAdapter(Context context, List<String> id, List<String> image, List<String> name, List<String> status, List<String> contactPhone, List<String> email) {
        this.context = context;
        this.id = id;
        this.image = image;
        this.name = name;
        this.status = status;
        this.contactPhone = contactPhone;
        this.email = email;
    }


    @Override
    public int getCount() {
        return id.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.report_detail_record_list,parent, false);
            viewHolder.id = (TextView) convertView.findViewById(R.id.reportDetailId);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.reportDetailImage);
            viewHolder.name = (TextView) convertView.findViewById(R.id.reportDetailName);
            viewHolder.status = (TextView) convertView.findViewById(R.id.reportDetailStatus);
            viewHolder.contactPhone = (TextView) convertView.findViewById(R.id.reportDetailContactPhone);
            viewHolder.email = (TextView) convertView.findViewById(R.id.reportDetailEmail);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.id.setText(id.get(position));
        // Load image from URL to ImageView
        new DownLoadImageUtility(viewHolder.image).execute(image.get(position));
        viewHolder.name.setText(name.get(position));
        String statusData = status.get(position);
        viewHolder.status.setText("Status: " + statusData);
        viewHolder.contactPhone.setText(contactPhone.get(position));
        viewHolder.email.setText(email.get(position));

        // Change background base on report detail status
        if (statusData.equalsIgnoreCase("Reporting")) {
            convertView.setBackgroundColor(ReportStatus.COLOR_REPORTING);
        } else if(statusData.equalsIgnoreCase("Took action")) {
            convertView.setBackgroundColor(ReportStatus.COLOR_TOOK_ACTION);
        } else if(statusData.equalsIgnoreCase("Unresolved")) {
            convertView.setBackgroundColor(ReportStatus.COLOR_UNRESOLVED);
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView id;
        ImageView image;
        TextView name;
        TextView status;
        TextView contactPhone;
        TextView email;
    }
}
