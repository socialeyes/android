package nguonchhay.reasei.socialeyes.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nguonchhay.reasei.socialeyes.R;
import nguonchhay.reasei.socialeyes.utility.DownLoadImageUtility;

public class AuthorizeReportAdapter extends BaseAdapter {

    private Context context;
    private List<String> authorizeReportId;
    private List<String> images;
    private List<String> names;
    private List<String> contactPhones;

    public AuthorizeReportAdapter(Context context, List<String> authorizeReportId, List<String> images, List<String> names, List<String> contactPhones) {
        this.context = context;
        this.authorizeReportId = authorizeReportId;
        this.images = images;
        this.names = names;
        this.contactPhones = contactPhones;
    }


    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.authorize_record_list,parent, false);
            viewHolder.id = (TextView) convertView.findViewById(R.id.authorizeReportId);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.authorizeReportImage);
            viewHolder.name = (TextView) convertView.findViewById(R.id.authorizeReportName);
            viewHolder.contactPhone = (TextView) convertView.findViewById(R.id.authorizeReportContactPhone);
            viewHolder.chkSelectAuthorizeReport = (CheckBox) convertView.findViewById(R.id.chkSelectAuthorizeReport);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.id.setText(authorizeReportId.get(position));
        // Load image from URL to ImageView
        new DownLoadImageUtility(viewHolder.image).execute(images.get(position));
        viewHolder.name.setText(names.get(position));
        viewHolder.contactPhone.setText(contactPhones.get(position));
        viewHolder.chkSelectAuthorizeReport.setChecked(false);

        return convertView;
    }

    private static class ViewHolder {
        TextView id;
        ImageView image;
        TextView name;
        TextView contactPhone;
        CheckBox chkSelectAuthorizeReport;
    }
}
