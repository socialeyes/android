package nguonchhay.reasei.socialeyes.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import nguonchhay.reasei.socialeyes.R;
import nguonchhay.reasei.socialeyes.fragment.ReporterBasicInfoFragment;
import nguonchhay.reasei.socialeyes.fragment.ReporterCredentialFragment;
import nguonchhay.reasei.socialeyes.model.Reporter;

public class ReporterFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private Reporter reporter;

    public ReporterFragmentPagerAdapter(Context context, FragmentManager fm, Reporter reporter) {
        super(fm);
        mContext = context;
        this.reporter = reporter;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return ReporterBasicInfoFragment.newInstance(reporter);
        } else {
            return ReporterCredentialFragment.newInstance(reporter);
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 2;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return mContext.getString(R.string.reporter_tab_basic_info);
            case 1:
                return mContext.getString(R.string.reporter_tab_credential);
            default:
                return null;
        }
    }
}
