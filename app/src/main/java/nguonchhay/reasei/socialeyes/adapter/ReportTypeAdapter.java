package nguonchhay.reasei.socialeyes.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.List;

import nguonchhay.reasei.socialeyes.R;
import nguonchhay.reasei.socialeyes.model.ReportType;
import nguonchhay.reasei.socialeyes.utility.DownLoadImageUtility;

public class ReportTypeAdapter extends ArrayAdapter<ReportType> {

    private Context context;
    private List<ReportType> listReporterType;

    public ReportTypeAdapter(Context context, List<ReportType> listReporterType) {
        super(context, R.layout.report_type_list, listReporterType);
        this.context = context;
        this.listReporterType = listReporterType;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.report_type_list, parent, false);
        ReportType reporterType = (ReportType) listReporterType.get(position);

        // Load image from URL to ImageView
        new DownLoadImageUtility((ImageView) view.findViewById(R.id.reportTypeImage))
                .execute(reporterType.getImage());

        return view;
    }


}
