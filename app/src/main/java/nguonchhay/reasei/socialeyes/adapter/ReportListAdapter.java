package nguonchhay.reasei.socialeyes.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nguonchhay.reasei.socialeyes.R;
import nguonchhay.reasei.socialeyes.model.ReportStatus;
import nguonchhay.reasei.socialeyes.model.ReportType;
import nguonchhay.reasei.socialeyes.utility.DownLoadImageUtility;

public class ReportListAdapter extends BaseAdapter {

    private Context context;
    private List<String> reportId;
    private List<String> image;
    private List<String> reportStatusName;
    private List<ReportType> reportType;

    public ReportListAdapter(Context context, List<String> reportId, List<String> reportStatusName, List<ReportType> reportType, List<String> image) {
        this.context = context;
        this.reportId = reportId;
        this.image = image;
        this.reportStatusName = reportStatusName;
        this.reportType = reportType;
    }


    @Override
    public int getCount() {
        return reportId.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.report_list,parent, false);
            viewHolder.id = (TextView) convertView.findViewById(R.id.reportId);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.reportImage);
            viewHolder.reportStatusName = (TextView) convertView.findViewById(R.id.reportStatus);
            viewHolder.reportTypeName = (TextView) convertView.findViewById(R.id.reportType);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.id.setText(reportId.get(position));

        ReportType reportTypeData = (ReportType) reportType.get(position);

        // Load image from URL to ImageView
        String reportImageLink = image.get(position);
        if (reportImageLink.equalsIgnoreCase("")) {
            new DownLoadImageUtility(viewHolder.image).execute(reportTypeData.getImage());
        } else {
            new DownLoadImageUtility(viewHolder.image).execute(reportImageLink);
        }

        viewHolder.reportTypeName.setText("Type: " + reportTypeData.getName());

        String reportStatusNameData = reportStatusName.get(position);
        viewHolder.reportStatusName.setText("Status: " + reportStatusNameData);

        // Change background base on report status
        if (reportStatusNameData.equalsIgnoreCase("Reporting")) {
            convertView.setBackgroundColor(ReportStatus.COLOR_REPORTING);
        } else if(reportStatusNameData.equalsIgnoreCase("Took action")) {
            convertView.setBackgroundColor(ReportStatus.COLOR_TOOK_ACTION);
        } else if(reportStatusNameData.equalsIgnoreCase("Unresolved")) {
            convertView.setBackgroundColor(ReportStatus.COLOR_UNRESOLVED);
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView id;
        ImageView image;
        TextView reportStatusName;
        TextView reportTypeName;
    }
}
