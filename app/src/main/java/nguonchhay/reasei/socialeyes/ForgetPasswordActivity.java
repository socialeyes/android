package nguonchhay.reasei.socialeyes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Order;
import java.util.List;

import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.LoginClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity implements Validator.ValidationListener {

    private Validator formValidator;
    private APIService apiService;

    @Order(1)
    @Email
    private EditText editEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_back);

        editEmail = (EditText) findViewById(R.id.editEmail);

        formValidator = new Validator(this);
        formValidator.setValidationListener(this);
        apiService = new APIService();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigateUp(){
        finish();
        return true;
    }

    public void onSendResetPassword(View v) {
        formValidator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        String email = editEmail.getText().toString();

        LoginClient loginClient = apiService.createClientWithFormUrlEncoded(LoginClient.class, APIService.getApiTokenForRequest());
        loginClient.sendResetPasswordCode(email).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                if (statusCode == HttpUtility.STATUS_CODE_OK) {
                    Intent intent = new Intent(ForgetPasswordActivity.this, ResetPasswordActivity.class);
                    String email = editEmail.getText().toString();
                    intent.putExtra("email", email);
                    startActivity(intent);
                } else if (statusCode == HttpUtility.STATUS_CODE_REQUIRED_FIELD) {
                    Toast.makeText(ForgetPasswordActivity.this, "Fields are required", Toast.LENGTH_LONG).show();
                } else if (statusCode == HttpUtility.STATUS_CODE_UNPROCESSABLE_ENTITY) {
                    Toast.makeText(ForgetPasswordActivity.this, "Email does not exist. Do you really have account with us?", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(ForgetPasswordActivity.this, "Cannot make an request to server.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(ForgetPasswordActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
