package nguonchhay.reasei.socialeyes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nguonchhay.reasei.socialeyes.adapter.AuthorizeReportAdapter;
import nguonchhay.reasei.socialeyes.database.ReporterContract;
import nguonchhay.reasei.socialeyes.database.SocialEyeDbHelper;
import nguonchhay.reasei.socialeyes.model.Reporter;
import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.AuthorizeReportClient;
import nguonchhay.reasei.socialeyes.service.ReportClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class AuthorizeReportActivity extends AppCompatActivity {

    private ListView listAuthorizeReport;
    private AuthorizeReportAdapter authorizeReportAdapter;
    private APIService apiService = new APIService();
    private List<String> selectedItemsFromList;

    private Reporter reporter;
    private int reportTypeId;
    private String reportAudioFilename;
    private String reportImageFilename;
    private String reportDescription;

    private SocialEyeDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorize_report);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_back);

        Intent intent = getIntent();
        reportTypeId = Integer.parseInt(intent.getStringExtra("reportTypeId"));
        reportAudioFilename = intent.getStringExtra("audioFileName");
        reportImageFilename = intent.getStringExtra("imageFileName");
        reportDescription = intent.getStringExtra("description");

        dbHelper = new SocialEyeDbHelper(this);

        // Get logged in reporter
        reporter = ReporterContract.getReporter(dbHelper.getReadableDatabase());
        if (reporter.getRememberToken() == null) {
            Intent intentLogin = new Intent(AuthorizeReportActivity.this, LoginActivity.class);
            startActivity(intentLogin);
        }

        /**
         * Save / delete selected authorize report from ListView
         */
        selectedItemsFromList = new ArrayList<>();
        listAuthorizeReport = (ListView) findViewById(R.id.listAuthorizeReport);
        listAuthorizeReport.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckBox cb = (CheckBox) view.findViewById(R.id.chkSelectAuthorizeReport);
                cb.setChecked(!cb.isChecked());

                TextView text = (TextView) view.findViewById(R.id.authorizeReportId);
                String authorizeReportId = text.getText().toString();
                if (cb.isChecked()) {
                    selectedItemsFromList.add(authorizeReportId);
                } else {
                    selectedItemsFromList.remove(authorizeReportId);
                }
            }
        });

        /**
         * Get all authorize report data from server
         */
        AuthorizeReportClient authorizeReportClient = apiService.createClientWithJson(AuthorizeReportClient.class, APIService.getApiTokenForRequest());
        authorizeReportClient.getAuthorizeReport(reportTypeId)
            .enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    int statusCode = response.code();
                    if (statusCode == HttpUtility.STATUS_CODE_OK) {
                        try {
                            String json = response.body().string();
                            JSONObject jsonObject = new JSONObject(json);
                            JSONArray responseData = new JSONArray(jsonObject.optString("data"));

                            List<String> authorizeReportId = new ArrayList<>();
                            List<String> images = new ArrayList<>();
                            List<String> names = new ArrayList<>();
                            List<String> contactPhones = new ArrayList<>();

                            for (int i =0; i < responseData.length(); i++) {
                                JSONObject row = responseData.getJSONObject(i);
                                authorizeReportId.add(row.getInt("id") + "");

                                JSONObject joAuthorizePlace = new JSONObject(row.getString("authorize_place"));
                                images.add(joAuthorizePlace.getString("logo"));
                                names.add(joAuthorizePlace.getString("name"));
                                contactPhones.add(joAuthorizePlace.getString("contact_phone"));

                            }

                            // Add data to ListView
                            authorizeReportAdapter = new AuthorizeReportAdapter(
                                    AuthorizeReportActivity.this,
                                    authorizeReportId,
                                    images,
                                    names,
                                    contactPhones
                            );
                            listAuthorizeReport.setAdapter(authorizeReportAdapter);
                        } catch (JSONException e) {
                            Log.d(TAG, "onResponse JSONException: " + e.getMessage());
                        } catch (IOException e) {
                            Log.d(TAG, "onResponse IOException: " + e.getMessage());
                        }
                    } else {
                        Toast.makeText(AuthorizeReportActivity.this, HttpUtility.MESSAGE_PROCESS_DATA_ERROR, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(AuthorizeReportActivity.this, HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_LONG).show();
                }
            });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, ReportActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Send report to server
     */
    public void onSendReport(View v) {
        if (selectedItemsFromList.size() > 0) {
            String strAuthorizeReport = TextUtils.join(",", selectedItemsFromList);
            String accessToken = reporter.getRememberToken();

            ReportClient reportClient = apiService.createClientWithFormUrlEncoded(ReportClient.class, APIService.getAccessTokenForRequest(accessToken));
            reportClient.store(
                reporter.getId(), reportTypeId, strAuthorizeReport, reportAudioFilename, reportImageFilename, reportDescription
            ).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == HttpUtility.STATUS_CODE_OK) {
                        Intent intent = new Intent(AuthorizeReportActivity.this, ReportListActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(AuthorizeReportActivity.this, HttpUtility.MESSAGE_SERVER_ERROR, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(AuthorizeReportActivity.this, HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(AuthorizeReportActivity.this, "Please select at least one authorize people to report", Toast.LENGTH_LONG).show();
        }
    }
}
