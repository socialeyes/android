package nguonchhay.reasei.socialeyes.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AuthorizeReportClient {

    @GET("/api/v1/authorize_reports")
    Call<ResponseBody> getAuthorizeReport(@Query("report_type_id") int reportTypeId);
}
