package nguonchhay.reasei.socialeyes.service;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import nguonchhay.reasei.socialeyes.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Source: https://medium.com/@tsaha.cse/advanced-retrofit2-part-1-network-error-handling-response-caching-77483cf68620
 */
public class APIService {

    private static final int CONNECT_TIMEOUT = 30;
    private static final int READ_TIMEOUT = 30;
    private static final int WRITE_TIMEOUT = 30;

    private String requestHeaderToken;
    private String headerContentType;

    private OkHttpClient buildOkHttpClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder.connectTimeout(APIService.CONNECT_TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.readTimeout(APIService.READ_TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.writeTimeout(APIService.WRITE_TIMEOUT, TimeUnit.SECONDS);

        okHttpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                // Request customization: add request headers .addHeader("Content-Type", headerContentType)
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Authorization", requestHeaderToken);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        return okHttpClientBuilder.build();
    }

    private Retrofit buildRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(APIService.getBaseUrl())
                .client(buildOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static String getBaseUrl() {
        return BuildConfig.BASE_URL;
    }

    public static String getApiTokenForRequest() {
        return "Bearer " + BuildConfig.API_TOKEN;
    }

    public static String getAccessTokenForRequest(String $accessToken) {
        return "Bearer " + $accessToken;
    }

    /**
     * Create all API client class with "application/json"
     */
    public <T> T createClientWithJson(final Class<T> service, String requestHeaderToken) {
        this.requestHeaderToken = requestHeaderToken;
        this.headerContentType = "application/json";
        Retrofit retrofit = buildRetrofit();
        return retrofit.create(service);
    }

    /**
     * Create all API client class "application/x-www-form-urlencoded"
     */
    public <T> T createClientWithFormUrlEncoded(final Class<T> service, String requestHeaderToken) {
        this.requestHeaderToken = requestHeaderToken;
        this.headerContentType = "application/x-www-form-urlencoded";
        Retrofit retrofit = buildRetrofit();
        return retrofit.create(service);
    }

    /**
     * Create all API client class
     */
    public <T> T createClient(final Class<T> service, String requestHeaderToken) {
        this.requestHeaderToken = requestHeaderToken;
        Retrofit retrofit = buildRetrofit();
        return retrofit.create(service);
    }
}
