package nguonchhay.reasei.socialeyes.service;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface FileClient {

    @Multipart
    @POST("/api/v1/file_upload")
    Call<ResponseBody> uploadFile(@Part MultipartBody.Part file);
}
