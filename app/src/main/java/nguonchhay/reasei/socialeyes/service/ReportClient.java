package nguonchhay.reasei.socialeyes.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ReportClient {

    @GET("/api/v1/reports/{reporter_id}")
    Call<ResponseBody> list(
        @Path("reporter_id") int id,
        @Query("limit") int limit,
        @Query("page[number]") int pageNumber
    );

    @FormUrlEncoded
    @POST("/api/v1/reports")
    Call<ResponseBody> store(
        @Field("reporter_id") int reporterId,
        @Field("report_type_id") int reportTypeId,
        @Field("authorize_reports") String authorizeReports,
        @Field("audio") String audio,
        @Field("image") String image,
        @Field("description") String description
    );

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "/api/v1/reports", hasBody = true)
    Call<ResponseBody> delete(
        @Field("reporter_id") int reporterId,
        @Field("report_id") int reportId,
        @Field("delete_reason") String deleteReason
    );
}
