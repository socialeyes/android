package nguonchhay.reasei.socialeyes.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ReporterClient {

    @FormUrlEncoded
    @PUT("/api/v1/reporters/{id}/update_credential")
    Call<ResponseBody> updateCredential(
        @Path("id") int id,
        @Field("password") String password,
        @Field("confirm_password") String confirmPassword
    );

    @FormUrlEncoded
    @PUT("/api/v1/reporters/{id}/update_info")
    Call<ResponseBody> updateInfo(
            @Path("id") int id,
            @Field("sex") String sex,
            @Field("birth_date") String birthDate,
            @Field("address") String address,
            @Field("image") String image,
            @Field("first_name") String firstName,
            @Field("last_name") String lastName
    );
}
