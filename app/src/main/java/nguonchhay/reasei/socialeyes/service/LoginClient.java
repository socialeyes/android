package nguonchhay.reasei.socialeyes.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginClient {

    @POST("/api/v1/authorization/check_user_session")
    Call<ResponseBody> checkUserSession();

    @FormUrlEncoded
    @POST("/api/v1/registers")
    Call<ResponseBody> register(
            @Field("email") String email,
            @Field("password") String password,
            @Field("sex") String sex,
            @Field("birth_date") String birthDate,
            @Field("address") String address
    );

    @FormUrlEncoded
    @POST("/api/v1/registers/verify_code")
    Call<ResponseBody> registerVerify(
            @Field("reporter_id") int id,
            @Field("verify_code") String verifyCode
    );

    @FormUrlEncoded
    @POST("/api/v1/authorization/login")
    Call<ResponseBody> login(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("/api/v1/authorization/send_reset_password_code")
    Call<ResponseBody> sendResetPasswordCode(@Field("email") String email);

    @FormUrlEncoded
    @POST("/api/v1/authorization/reset_password")
    Call<ResponseBody> resetPassword(
            @Field("email") String email,
            @Field("verify_code") String verifyCode,
            @Field("password") String password
    );
}
