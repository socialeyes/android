package nguonchhay.reasei.socialeyes.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ReportTypeClient {

    @GET("/api/v1/report_types")
    Call<ResponseBody> list();
}
