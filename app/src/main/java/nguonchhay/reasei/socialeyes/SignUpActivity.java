package nguonchhay.reasei.socialeyes;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Order;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.LoginClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class SignUpActivity extends AppCompatActivity implements ValidationListener {

    private DatePickerDialog.OnDateSetListener dateSetListener;
    private Validator formValidator;
    private APIService apiService;

    @Order(1)
    @Email
    private EditText editEmail;

    @Order(2)
    @Password(min = 8, scheme = Password.Scheme.ALPHA_NUMERIC_MIXED_CASE_SYMBOLS, message = "Password should contain alpha lowercase and upper case, number, and symbol")
    private EditText editPassword;

    private RadioGroup gradioSex;

    @Order(4)
    @NotEmpty(message = "Birth date is required.")
    private EditText editBirthDate;

    @Order(5)
    @NotEmpty(message = "Address is required.")
    private EditText editAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_back);

        editEmail = (EditText) findViewById(R.id.editEmail);
        editPassword = (EditText) findViewById(R.id.editPassword);
        gradioSex = (RadioGroup) findViewById(R.id.radioGroupSex);
        editBirthDate = (EditText) findViewById(R.id.editBirthDate);
        editBirthDate.setFocusable(false);
        editAddress = (EditText) findViewById(R.id.editAddress);

        formValidator = new Validator(this);
        formValidator.setValidationListener(this);
        apiService = new APIService();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigateUp(){
        finish();
        return true;
    }

    public void onDatePicker(View v) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR) - 10;
        int month = calendar.get(Calendar.MONTH);
        int day = 1;

        DatePickerDialog datePickerDialog = new DatePickerDialog(
            SignUpActivity.this,
            android.R.style.Theme_Holo_Light_Dialog_MinWidth,
            dateSetListener,
            year, month, day
        );

        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        datePickerDialog.show();

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month += 1;
                String selectedDate = dayOfMonth > 9 ? "" + dayOfMonth : "0" + dayOfMonth;
                selectedDate += "." + (month > 9 ? "" + month : "0" + month) + "." + year;
                editBirthDate.setText(selectedDate);
            }
        };
    }

    @Override
    public void onValidationSucceeded() {
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();
        RadioButton radioSex = (RadioButton) findViewById(gradioSex.getCheckedRadioButtonId());
        String sex = radioSex.getText().toString();
        String birthDate = editBirthDate.getText().toString();
        String dateToStore = adjustDateToStore(birthDate);
        String address = editAddress.getText().toString();

        LoginClient loginClient = apiService.createClientWithFormUrlEncoded(LoginClient.class, APIService.getApiTokenForRequest());
        loginClient.register(
            email, password, sex, dateToStore, address
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                if (statusCode == HttpUtility.STATUS_CODE_OK) {
                    try {
                        String json = response.body().string();
                        JSONObject jsonObject = new JSONObject(json);
                        JSONObject userInfo = new JSONObject(jsonObject.optString("data"));
                        int id = userInfo.getInt("id");

                        Intent intent = new Intent(SignUpActivity.this, SignUpVerificationActivity.class);
                        intent.putExtra("id", id + "");
                        startActivity(intent);
                    } catch (JSONException e) {
                        Log.d(TAG, "onResponse JSONException: " + e.getMessage());
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse IOException: " + e.getMessage());
                    }
                } else if (statusCode == HttpUtility.STATUS_CODE_REQUIRED_FIELD) {
                    Toast.makeText(SignUpActivity.this, "Fields are required", Toast.LENGTH_LONG).show();
                } else if (statusCode == HttpUtility.STATUS_CODE_UNPROCESSABLE_ENTITY) {
                    Toast.makeText(SignUpActivity.this, "Email already existed.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(SignUpActivity.this, HttpUtility.MESSAGE_SERVER_ERROR, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(SignUpActivity.this, HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onSignUp(View v) {
        formValidator.validate();
    }

    public String adjustDateToStore(String date) {
        String[] parts = date.split("\\.", 3);
        StringBuilder storeDate = new StringBuilder();
        storeDate.append(parts[2]).append(".").append(parts[1]).append(".").append(parts[0]);
        return storeDate.toString();
    }
}
