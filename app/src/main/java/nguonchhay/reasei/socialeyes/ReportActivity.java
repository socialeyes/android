package nguonchhay.reasei.socialeyes;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import nguonchhay.reasei.socialeyes.model.Report;
import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.FileClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import nguonchhay.reasei.socialeyes.utility.ImageUtility;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class ReportActivity extends AppCompatActivity {

    private String reportTypeId;
    private ImageView imageTakePhoto;
    private ImageView imageAudio;
    private EditText editDescription;
    private Button btnDeleteImage;
    private Button btnDeleteAudio;
    private TextView textAudioRecord;

    private APIService apiService = new APIService();
    private String reportAudioFileName = "";
    private String reportImageFileName = "";

    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int REQUEST_RECORD_IMAGE_PERMISSION = 1337;
    private String [] appPermissions = {
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    };

    public static final int INTENT_AUDIO_RECORD_REQUEST_CODE = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_back);

        editDescription = (EditText) findViewById(R.id.editReportText);
        textAudioRecord = (TextView) findViewById(R.id.textAudioRecord);
        textAudioRecord.setText("Audio record");

        reportTypeId = getIntent().getStringExtra("reportTypeId");

        btnDeleteAudio = (Button) findViewById(R.id.btnDeleteAudio);
        btnDeleteAudio.setVisibility(View.GONE);

        btnDeleteImage = (Button) findViewById(R.id.btnDeleteImage);
        btnDeleteImage.setVisibility(View.GONE);

        imageTakePhoto = (ImageView) findViewById(R.id.imageTakePhoto);
        imageTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (ContextCompat.checkSelfPermission(ReportActivity.this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ReportActivity.this, appPermissions, REQUEST_RECORD_IMAGE_PERMISSION);
            } else {
                btnDeleteImage.setVisibility(View.GONE);
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
            }
        });

        imageAudio = (ImageView) findViewById(R.id.imageAudio);
        imageAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent audioIntent = new Intent(ReportActivity.this, AudioRecordActivity.class);
                audioIntent.putExtra("reportTypeId", reportTypeId);
                startActivityForResult(audioIntent, INTENT_AUDIO_RECORD_REQUEST_CODE);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_RECORD_IMAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    btnDeleteImage.setVisibility(View.GONE);
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageTakePhoto.setImageBitmap(ImageUtility.compressBitmap(imageBitmap, 50, true));

            btnDeleteImage.setVisibility(View.VISIBLE);

            // Save image from image view
            reportImageFileName = ImageUtility.saveImageToDisk(imageTakePhoto, Report.TEMP_PATH, Report.TEMP_IMAGE_FILENAME);
        } else if (requestCode == INTENT_AUDIO_RECORD_REQUEST_CODE) {
            reportAudioFileName = data.getStringExtra("savedAudioFileName");
            if (!reportAudioFileName.equalsIgnoreCase("")) {
                btnDeleteAudio.setVisibility(View.VISIBLE);
                textAudioRecord.setText("Audio was recorded");
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onDeleteAudio(View v) {
        btnDeleteAudio.setVisibility(View.GONE);
        textAudioRecord.setText("Audio record");
        reportAudioFileName = "";
    }

    public void onDeleteImage(View v) {
        imageTakePhoto.setImageResource(R.drawable.take_photo);
        btnDeleteImage.setVisibility(View.GONE);
        reportImageFileName = "";
    }

    public void startAuthorizeReportActivity() {
        Intent intent = new Intent(ReportActivity.this, AuthorizeReportActivity.class);
        intent.putExtra("reportTypeId", reportTypeId + "");
        intent.putExtra("audioFileName", reportAudioFileName);
        intent.putExtra("imageFileName", reportImageFileName);
        intent.putExtra("description", editDescription.getText().toString());
        startActivity(intent);
    }

    public void onNextReport(View v) {
        String reportDescription = editDescription.getText().toString().trim();
        if (reportAudioFileName.equalsIgnoreCase("") && reportImageFileName.equalsIgnoreCase("") && reportDescription.equalsIgnoreCase("")) {
            Toast.makeText(ReportActivity.this, "Please provide any report (audio | image | description)", Toast.LENGTH_LONG).show();
        } else {
            if (!reportImageFileName.equalsIgnoreCase("")) {
                File file = new File(reportImageFileName);
                RequestBody imageFilePart = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part bodyFile = MultipartBody.Part.createFormData("file", file.getName(), imageFilePart);

                /**
                 * Pre-upload image to server then we will use only image string path to server for next request
                 */
                FileClient fileClient = apiService.createClient(FileClient.class, APIService.getApiTokenForRequest());
                fileClient.uploadFile(bodyFile).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        int statusCode = response.code();
                        if (statusCode == HttpUtility.STATUS_CODE_OK) {
                            try {
                                String json = response.body().string();
                                JSONObject jsonObject = new JSONObject(json);
                                JSONObject responseData = new JSONObject(jsonObject.optString("data"));
                                reportImageFileName = responseData.getString("file");
                                startAuthorizeReportActivity();
                            } catch (JSONException e) {
                                Log.d(TAG, "onResponse JSONException: " + e.getMessage());
                            } catch (IOException e) {
                                Log.d(TAG, "onResponse IOException: " + e.getMessage());
                            }
                        } else {
                            Toast.makeText(ReportActivity.this, HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(ReportActivity.this, HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                reportImageFileName = "";
                reportAudioFileName = "";
                startAuthorizeReportActivity();
            }
        }
    }
}
