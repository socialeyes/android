package nguonchhay.reasei.socialeyes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import java.util.List;

import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.LoginClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class SignUpVerificationActivity extends AppCompatActivity implements ValidationListener {

    private Validator formValidator;
    private APIService apiService;
    private int reporterId;

    @NotEmpty(message = "Verify code is required.")
    @Length(min = 6, max = 6, message = "Please provide only six digits.")
    private EditText editVerifyCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_verification);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_back);

        editVerifyCode = (EditText) findViewById(R.id.editVerifyCode);

        reporterId = Integer.parseInt(getIntent().getStringExtra("id"));

        formValidator = new Validator(this);
        formValidator.setValidationListener(this);
        apiService = new APIService();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, SignUpActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onVerifyCode(View v) {
        formValidator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        String verifyCode = editVerifyCode.getText().toString();
        LoginClient loginClient = apiService.createClientWithFormUrlEncoded(LoginClient.class, APIService.getApiTokenForRequest());
        loginClient.registerVerify(reporterId, verifyCode)
            .enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    int statusCode = response.code();
                    if (statusCode == HttpUtility.STATUS_CODE_OK) {
                        try {
                            Intent intent = new Intent(SignUpVerificationActivity.this, LoginActivity.class);
                            startActivity(intent);
                        } catch (Exception e) {
                            Log.d(TAG, "onResponse JSONException: " + e.getMessage());
                        }
                    } else if (statusCode == HttpUtility.STATUS_CODE_NOT_MODIFIED) {
                        Toast.makeText(SignUpVerificationActivity.this, "Verify code is incorrect.", Toast.LENGTH_LONG).show();
                    } else if (statusCode == HttpUtility.STATUS_CODE_REQUIRED_FIELD) {
                        Toast.makeText(SignUpVerificationActivity.this, "Fields are required.", Toast.LENGTH_LONG).show();
                    } else if (statusCode == HttpUtility.STATUS_CODE_NOT_FOUND) {
                        Toast.makeText(SignUpVerificationActivity.this, "Registered report was not found.", Toast.LENGTH_LONG).show();
                    } else if(statusCode == HttpUtility.STATUS_CODE_REQUEST_TIMEOUT) {
                        Toast.makeText(SignUpVerificationActivity.this, "Verify code was expired. We sent you a new verify code. Please check again.", Toast.LENGTH_LONG).show();
                    } else if (statusCode == HttpUtility.STATUS_CODE_UNPROCESSABLE_ENTITY) {
                        Toast.makeText(SignUpVerificationActivity.this, "ReporterContract was already activated.", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(SignUpVerificationActivity.this, "Cannot make an request to server.", Toast.LENGTH_SHORT).show();
                }
            });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(SignUpVerificationActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
