package nguonchhay.reasei.socialeyes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nguonchhay.reasei.socialeyes.adapter.ReportListAdapter;
import nguonchhay.reasei.socialeyes.database.ReportContract;
import nguonchhay.reasei.socialeyes.database.ReportDetailContract;
import nguonchhay.reasei.socialeyes.database.ReporterContract;
import nguonchhay.reasei.socialeyes.database.SocialEyeDbHelper;
import nguonchhay.reasei.socialeyes.model.Report;
import nguonchhay.reasei.socialeyes.model.ReportDetail;
import nguonchhay.reasei.socialeyes.model.Reporter;
import nguonchhay.reasei.socialeyes.model.ReportType;
import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.ReportClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class ReportListActivity extends AppCompatActivity {

    private ListView listReport;
    private ReportListAdapter reportListAdapter;

    private APIService apiService = new APIService();
    private SocialEyeDbHelper dbHelper;

    private int numberOfPagination = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_list);

        dbHelper = new SocialEyeDbHelper(this);

        // Get logged in reporter
        Reporter reporter = ReporterContract.getReporter(dbHelper.getReadableDatabase());
        if (reporter.getRememberToken() == null) {
            Intent intentLogin = new Intent(ReportListActivity.this, LoginActivity.class);
            startActivity(intentLogin);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_back);

        int reportListLimit = BuildConfig.REPORT_LIST_LIMIT;
        int reportListNumPaginate = BuildConfig.REPORT_LIST_NUM_PAGINATE;

        listReport = (ListView) findViewById(R.id.listReport);
        registerForContextMenu(listReport);
        listReport.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textReport = (TextView) view.findViewById(R.id.reportId);
                String reportId = textReport.getText().toString();

                Intent intent = new Intent(ReportListActivity.this, ReportListDetailActivity.class);
                intent.putExtra("reportId", reportId);
                startActivity(intent);
            }
        });

        /**
         * Get all authorize report data from server
         */
        ReportClient reportClient = apiService.createClientWithJson(ReportClient.class, APIService.getAccessTokenForRequest(reporter.getRememberToken()));
        reportClient.list(reporter.getId(), reportListLimit, reportListNumPaginate)
            .enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    int statusCode = response.code();
                    if (statusCode == HttpUtility.STATUS_CODE_OK) {
                        try {
                            String json = response.body().string();
                            JSONObject jsonObject = new JSONObject(json);
                            JSONObject responseData = jsonObject.getJSONObject("data");

                            numberOfPagination = Integer.parseInt(responseData.getString("numPaginate"));

                            List<String> reportId = new ArrayList<>();
                            List<String> image = new ArrayList<>();
                            List<String> reportStatusName = new ArrayList<>();
                            List<ReportType> reportType = new ArrayList<>();

                            // Clean up report and report detail data from table
                            ReportContract.cleanTable(dbHelper.getWritableDatabase());
                            ReportDetailContract.cleanTable(dbHelper.getWritableDatabase());

                            JSONArray responseReports = responseData.getJSONArray("reports");
                            for (int i = 0; i < responseReports.length(); i++) {
                                JSONObject reportData = responseReports.getJSONObject(i);
                                int reportIdData = reportData.getInt("id");
                                reportId.add(reportIdData + "");

                                JSONObject reportStatusData = reportData.getJSONObject("report_status");
                                reportStatusName.add(reportStatusData.getString("name"));

                                JSONObject reportTypeData = reportData.getJSONObject("report_type");
                                ReportType objReportType = new ReportType(
                                    Integer.parseInt(reportTypeData.getString("id")),
                                    reportTypeData.getString("name"),
                                    reportTypeData.getString("image")
                                );
                                reportType.add(objReportType);

                                String reportImage = reportData.getString("image");
                                image.add(reportImage);

                                Report objReport = new Report(
                                        reportIdData,
                                        reportData.getString("audio"),
                                        reportImage,
                                        reportData.getString("description")
                                );
                                Long resultReport = ReportContract.insert(dbHelper.getWritableDatabase(), objReport);

                                // Save report details to database
                                JSONArray responseReportDetails = reportData.getJSONArray("report_details");
                                for (int k = 0; k < responseReportDetails.length(); k++) {
                                    JSONObject reportDetailData = responseReportDetails.getJSONObject(k);
                                    ReportDetail objReportDetail = new ReportDetail(
                                            reportDetailData.getInt("id"),
                                            reportIdData,
                                            reportDetailData.getJSONObject("report_status").toString(),
                                            reportDetailData.getJSONObject("authorize_report").toString(),
                                            reportDetailData.getString("response_datetime")
                                    );
                                    Long resultReportDetail = ReportDetailContract.insert(dbHelper.getWritableDatabase(), objReportDetail);
                                }
                            }

                            reportListAdapter = new ReportListAdapter(
                                ReportListActivity.this,
                                    reportId,
                                    reportStatusName,
                                    reportType,
                                    image
                            );
                            listReport.setAdapter(reportListAdapter);

                        } catch (JSONException e) {
                            Log.d(TAG, "onResponse JSONException: " + e.getMessage());
                        } catch (IOException e) {
                            Log.d(TAG, "onResponse IOException: " + e.getMessage());
                        }
                    } else {
                        Toast.makeText(ReportListActivity.this, HttpUtility.MESSAGE_PROCESS_DATA_ERROR, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(ReportListActivity.this, HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_LONG).show();
                }
            });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Report list MENU
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.listReport) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_report_actions_list, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch(item.getItemId()) {
            case R.id.report_list_action_delete:
                TextView textReportId = (TextView) menuInfo.targetView.findViewById(R.id.reportId);
                if (textReportId != null) {
                    Intent intent = new Intent(ReportListActivity.this, ReportDeleteActivity.class);
                    intent.putExtra("reportId", textReportId.getText().toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "System cannot detect the selected Report to delete.", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
