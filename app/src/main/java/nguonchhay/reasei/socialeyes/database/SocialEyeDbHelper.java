package nguonchhay.reasei.socialeyes.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SocialEyeDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "SocialEye.db";

    public SocialEyeDbHelper(Context context) {
        // Create database
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        // Create tables
        db.execSQL(ReporterContract.SQL_CREATE_ENTRIES);
        db.execSQL(ReportContract.SQL_CREATE_ENTRIES);
        db.execSQL(ReportDetailContract.SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(ReporterContract.SQL_DELETE_ENTRIES);
        db.execSQL(ReportContract.SQL_DELETE_ENTRIES);
        db.execSQL(ReportDetailContract.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
