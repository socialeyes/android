package nguonchhay.reasei.socialeyes.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import nguonchhay.reasei.socialeyes.model.Reporter;

public class ReporterContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private ReporterContract() {}

    /* Inner class that defines the table contents */
    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "reporters";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_FIRST_NAME = "first_name";
        public static final String COLUMN_NAME_LAST_NAME = "last_name";
        public static final String COLUMN_NAME_SEX = "sex";
        public static final String COLUMN_NAME_BIRTH_DATE = "birth_date";
        public static final String COLUMN_NAME_ADDRESS = "address";
        public static final String COLUMN_NAME_IMAGE = "image";
        public static final String COLUMN_NAME_ACTIVATE = "activate";
        public static final String COLUMN_NAME_STARTED_SESSION = "started_session";
        public static final String COLUMN_NAME_REMEMBER_TOKEN = "remember_token";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + FeedEntry.TABLE_NAME + " (" +
                    FeedEntry.COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
                    FeedEntry.COLUMN_NAME_EMAIL + " TEXT," +
                    FeedEntry.COLUMN_NAME_FIRST_NAME + " TEXT," +
                    FeedEntry.COLUMN_NAME_LAST_NAME + " TEXT," +
                    FeedEntry.COLUMN_NAME_SEX + " TEXT," +
                    FeedEntry.COLUMN_NAME_BIRTH_DATE + " TEXT," +
                    FeedEntry.COLUMN_NAME_ADDRESS + " TEXT," +
                    FeedEntry.COLUMN_NAME_IMAGE + " TEXT," +
                    FeedEntry.COLUMN_NAME_ACTIVATE + " INT," +
                    FeedEntry.COLUMN_NAME_STARTED_SESSION + " TEXT," +
                    FeedEntry.COLUMN_NAME_REMEMBER_TOKEN + " TEXT)"
            ;

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    /**
     * Insert new reporter to database
     */
    public static long insert(SQLiteDatabase db, Reporter reporter) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FeedEntry.COLUMN_NAME_ID, reporter.getId());
        contentValues.put(FeedEntry.COLUMN_NAME_EMAIL, reporter.getEmail());
        contentValues.put(FeedEntry.COLUMN_NAME_FIRST_NAME, reporter.getFirstName());
        contentValues.put(FeedEntry.COLUMN_NAME_LAST_NAME, reporter.getLastName());
        contentValues.put(FeedEntry.COLUMN_NAME_SEX, reporter.getSex());
        contentValues.put(FeedEntry.COLUMN_NAME_BIRTH_DATE, reporter.getBirthDate());
        contentValues.put(FeedEntry.COLUMN_NAME_ADDRESS, reporter.getAddress());
        contentValues.put(FeedEntry.COLUMN_NAME_IMAGE, reporter.getImage());
        contentValues.put(FeedEntry.COLUMN_NAME_ACTIVATE, reporter.getActivation());
        contentValues.put(FeedEntry.COLUMN_NAME_STARTED_SESSION, reporter.getStartedSession());
        contentValues.put(FeedEntry.COLUMN_NAME_REMEMBER_TOKEN, reporter.getRememberToken());

        long result = db.insert(FeedEntry.TABLE_NAME, null, contentValues);
        return result;
    }

    /**
     * Insert new reporter to database
     */
    public static long update(SQLiteDatabase db, Reporter reporter) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FeedEntry.COLUMN_NAME_FIRST_NAME, reporter.getFirstName());
        contentValues.put(FeedEntry.COLUMN_NAME_LAST_NAME, reporter.getLastName());
        contentValues.put(FeedEntry.COLUMN_NAME_SEX, reporter.getSex());
        contentValues.put(FeedEntry.COLUMN_NAME_BIRTH_DATE, reporter.getBirthDate());
        contentValues.put(FeedEntry.COLUMN_NAME_ADDRESS, reporter.getAddress());
        contentValues.put(FeedEntry.COLUMN_NAME_IMAGE, reporter.getImage());
        contentValues.put(FeedEntry.COLUMN_NAME_REMEMBER_TOKEN, reporter.getRememberToken());

        long result = db.update(
            FeedEntry.TABLE_NAME,
            contentValues,
            FeedEntry.COLUMN_NAME_ID + "=" + reporter.getId(),
            null
        );
        return result;
    }

    /**
     * Select reporter from database
     */
    public static Reporter getReporter(SQLiteDatabase db) {
        String query = String.format("SELECT * FROM %s LIMIT 1;", FeedEntry.TABLE_NAME);
        Cursor cursor = db.rawQuery(query, null);
        Reporter reporter = new Reporter();
        if (cursor.moveToFirst()) {
            reporter.setId(cursor.getInt(cursor.getColumnIndex(FeedEntry.COLUMN_NAME_ID)));
            reporter.setFirstName(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_NAME_FIRST_NAME)));
            reporter.setLastName(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_NAME_LAST_NAME)));
            reporter.setEmail(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_NAME_EMAIL)));
            reporter.setSex(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_NAME_SEX)));
            reporter.setBirthDate(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_NAME_BIRTH_DATE)));
            reporter.setAddress(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_NAME_ADDRESS)));
            reporter.setImage(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_NAME_IMAGE)));
            reporter.setActivation(cursor.getInt(cursor.getColumnIndex(FeedEntry.COLUMN_NAME_ACTIVATE)));
            reporter.setStartedSession(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_NAME_STARTED_SESSION)));
            reporter.setRememberToken(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_NAME_REMEMBER_TOKEN)));
        }
        cursor.close();
        return reporter;
    }

    /**
     * Delete all reporters from database
     */
    public static void cleanTable(SQLiteDatabase db) {
        db.execSQL("DELETE FROM " + FeedEntry.TABLE_NAME);
    }
}
