package nguonchhay.reasei.socialeyes.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

import nguonchhay.reasei.socialeyes.model.Report;
import nguonchhay.reasei.socialeyes.model.ReportDetail;

public class ReportContract {

    private ReportContract() {}

    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "reports";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_AUDIO = "audio";
        public static final String COLUMN_IMAGE = "image";
        public static final String COLUMN_DESCRIPTION = "description";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + ReportContract.FeedEntry.TABLE_NAME + " (" +
                    ReportContract.FeedEntry.COLUMN_ID + " INTEGER PRIMARY KEY," +
                    FeedEntry.COLUMN_AUDIO + " TEXT," +
                    FeedEntry.COLUMN_IMAGE + " TEXT," +
                    FeedEntry.COLUMN_DESCRIPTION + " TEXT)"
            ;

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + ReportContract.FeedEntry.TABLE_NAME;

    /**
     * Insert new report detail to database
     */
    public static long insert(SQLiteDatabase db, Report report) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ReportContract.FeedEntry.COLUMN_ID, report.getId());
        contentValues.put(FeedEntry.COLUMN_AUDIO, report.getAudio());
        contentValues.put(FeedEntry.COLUMN_IMAGE, report.getImage());
        contentValues.put(FeedEntry.COLUMN_DESCRIPTION, report.getDescription());

        long result = db.insert(ReportContract.FeedEntry.TABLE_NAME, null, contentValues);
        return result;
    }

    /**
     * Select reports from database
     */
    public static List<Report> getReports(SQLiteDatabase db) {
        String query = String.format("SELECT * FROM %s ;", ReportContract.FeedEntry.TABLE_NAME);
        Cursor cursor = db.rawQuery(query, null);
        List<Report> reports = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                Report report = new Report();
                report.setId(cursor.getInt(cursor.getColumnIndex(FeedEntry.COLUMN_ID)));
                report.setAudio(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_AUDIO)));
                report.setImage(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_IMAGE)));
                report.setDescription(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_DESCRIPTION)));
                reports.add(report);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return reports;
    }

    /**
     * Select reports from database
     */
    public static Report getReport(SQLiteDatabase db, int reportId) {
        String query = String.format("SELECT * FROM %s WHERE id=%s;", ReportContract.FeedEntry.TABLE_NAME, reportId);
        Cursor cursor = db.rawQuery(query, null);
        Report report = new Report();

        if (cursor.moveToFirst()) {
            report.setId(cursor.getInt(cursor.getColumnIndex(FeedEntry.COLUMN_ID)));
            report.setAudio(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_AUDIO)));
            report.setImage(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_IMAGE)));
            report.setDescription(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_DESCRIPTION)));
        }
        cursor.close();
        return report;
    }

    /**
     * Delete all reportDetails from database
     */
    public static void cleanTable(SQLiteDatabase db) {
        db.execSQL("DELETE FROM " + ReportContract.FeedEntry.TABLE_NAME);
    }
}
