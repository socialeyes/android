package nguonchhay.reasei.socialeyes.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import nguonchhay.reasei.socialeyes.model.Report;
import nguonchhay.reasei.socialeyes.model.ReportDetail;

import static android.content.ContentValues.TAG;

public class ReportDetailContract {

    private ReportDetailContract() {}

    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "report_details";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_REPORT_ID = "report_id";
        public static final String COLUMN_REPORT_STATUS_JSON = "report_status_json";
        public static final String COLUMN_AUTHORIZE_REPORT_JSON = "authorize_report_json";
        public static final String COLUMN_RESPONSE_DATETIME = "response_datetime";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + ReportDetailContract.FeedEntry.TABLE_NAME + " (" +
                    ReportDetailContract.FeedEntry.COLUMN_ID + " INTEGER PRIMARY KEY," +
                    ReportDetailContract.FeedEntry.COLUMN_REPORT_ID + " INTEGER," +
                    ReportDetailContract.FeedEntry.COLUMN_REPORT_STATUS_JSON + " TEXT," +
                    ReportDetailContract.FeedEntry.COLUMN_AUTHORIZE_REPORT_JSON + " TEXT," +
                    ReportDetailContract.FeedEntry.COLUMN_RESPONSE_DATETIME + " TEXT)"
            ;

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + ReportDetailContract.FeedEntry.TABLE_NAME;

    /**
     * Insert new report detail to database
     */
    public static long insert(SQLiteDatabase db, ReportDetail reportDetail) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ReportDetailContract.FeedEntry.COLUMN_ID, reportDetail.getId());
        contentValues.put(ReportDetailContract.FeedEntry.COLUMN_REPORT_ID, reportDetail.getReportId());
        contentValues.put(ReportDetailContract.FeedEntry.COLUMN_REPORT_STATUS_JSON, reportDetail.getReportStatusJson());
        contentValues.put(ReportDetailContract.FeedEntry.COLUMN_AUTHORIZE_REPORT_JSON, reportDetail.getAuthorizeReportJson());
        contentValues.put(ReportDetailContract.FeedEntry.COLUMN_RESPONSE_DATETIME, reportDetail.getResponseDateTime());

        long result = db.insert(ReportDetailContract.FeedEntry.TABLE_NAME, null, contentValues);
        return result;
    }

    /**
     * Select reportDetail from database
     */
    public static List<ReportDetail> getReportDetailsByReport(SQLiteDatabase db, int reportId) {
        String query = String.format("SELECT * FROM %s WHERE report_id=%s;", ReportDetailContract.FeedEntry.TABLE_NAME, reportId);
        Cursor cursor = db.rawQuery(query, null);

        List<ReportDetail> reportDetails = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                ReportDetail reportDetail = new ReportDetail();
                reportDetail.setId(cursor.getInt(cursor.getColumnIndex(FeedEntry.COLUMN_ID)));
                reportDetail.setId(cursor.getInt(cursor.getColumnIndex(FeedEntry.COLUMN_REPORT_ID)));
                reportDetail.setReportStatusJson(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_REPORT_STATUS_JSON)));
                reportDetail.setAuthorizeReportJson(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_AUTHORIZE_REPORT_JSON)));
                reportDetail.setResponseDateTime(cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_RESPONSE_DATETIME)));
                reportDetails.add(reportDetail);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return reportDetails;
    }

    /**
     * Delete all reportDetails from database
     */
    public static void cleanTable(SQLiteDatabase db) {
        db.execSQL("DELETE FROM " + ReportDetailContract.FeedEntry.TABLE_NAME);
    }
}
