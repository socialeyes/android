package nguonchhay.reasei.socialeyes.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Order;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import nguonchhay.reasei.socialeyes.LoginActivity;
import nguonchhay.reasei.socialeyes.MainActivity;
import nguonchhay.reasei.socialeyes.R;
import nguonchhay.reasei.socialeyes.ReportActivity;
import nguonchhay.reasei.socialeyes.database.ReporterContract;
import nguonchhay.reasei.socialeyes.database.SocialEyeDbHelper;
import nguonchhay.reasei.socialeyes.model.Report;
import nguonchhay.reasei.socialeyes.model.Reporter;
import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.FileClient;
import nguonchhay.reasei.socialeyes.service.ReporterClient;
import nguonchhay.reasei.socialeyes.utility.DownLoadImageUtility;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import nguonchhay.reasei.socialeyes.utility.ImageUtility;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

public class ReporterBasicInfoFragment extends Fragment
        implements Validator.ValidationListener {

    private DatePickerDialog.OnDateSetListener dateSetListener;
    private Validator formValidator;
    private APIService apiService;

    private int reporterId;
    private String accessToken;

    private String image;
    private String imageFileName;
    private String firstName;
    private String lastName;
    private String sex;
    private String birthDate;
    private String address;

    private ImageView imageReporter;
    private EditText editFirstName;
    private EditText editLastName;
    private RadioGroup gradioSex;

    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int REQUEST_IMAGE_BROWSER = 2;
    public static final int REQUEST_RECORD_IMAGE_PERMISSION = 1337;
    public static final int REQUEST_SELECT_IMAGE_PERMISSION = 1447;
    private String [] cameraPermissions = {
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    };

    private String [] browsePermissions = {
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    };

    @Order(1)
    @NotEmpty(message = "Birth date is required.")
    private EditText editBirthDate;

    @Order(2)
    @NotEmpty(message = "Address is required.")
    private EditText editAddress;

    public ReporterBasicInfoFragment() {
        // Required empty public constructor
    }

    // newInstance constructor for creating fragment with arguments
    public static ReporterBasicInfoFragment newInstance(Reporter reporter) {
        ReporterBasicInfoFragment reporterBasicInfoFragment = new ReporterBasicInfoFragment();
        Bundle args = new Bundle();

        args.putString("accessToken", reporter.getRememberToken());
        args.putInt("reporterId", reporter.getId());

        args.putString("image", reporter.getImage());
        args.putString("firstName", reporter.getFirstName());
        args.putString("lastName", reporter.getLastName());
        args.putString("sex", reporter.getSex());
        args.putString("birthDate", reporter.getDisplayBirthDate());
        args.putString("address", reporter.getAddress());

        reporterBasicInfoFragment.setArguments(args);
        return reporterBasicInfoFragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        accessToken = getArguments().getString("accessToken");
        reporterId = getArguments().getInt("reporterId");

        firstName = getArguments().getString("firstName", "");
        lastName = getArguments().getString("lastName", "");
        sex = getArguments().getString("sex", "");
        birthDate = getArguments().getString("birthDate", "");
        address = getArguments().getString("address", "");
        image = getArguments().getString("image", "");
        imageFileName = "";

        formValidator = new Validator(this);
        formValidator.setValidationListener(this);
        apiService = new APIService();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reporter_basic_info, container, false);

        imageReporter = (ImageView) rootView.findViewById(R.id.imageReporter);
        if (!image.equalsIgnoreCase("")) {
            new DownLoadImageUtility(imageReporter).execute(image);
        }
        imageFileName = "";

        editFirstName = (EditText) rootView.findViewById(R.id.editFirstName);
        editFirstName.setText(firstName);

        editLastName = (EditText) rootView.findViewById(R.id.editLastName);
        editLastName.setText(lastName);

        gradioSex = (RadioGroup) rootView.findViewById(R.id.radioGroupSex);
        if (sex.equalsIgnoreCase("Male")) {
            gradioSex.check(R.id.radioMale);
        } else if (sex.equalsIgnoreCase("Female")) {
            gradioSex.check(R.id.radioFemale);
        } else {
            gradioSex.check(R.id.radioOther);
        }

        editBirthDate = (EditText) rootView.findViewById(R.id.editBirthDate);
        editBirthDate.setFocusable(false);
        editBirthDate.setText(birthDate);
        editBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDatePicker(v);
            }
        });

        editAddress = (EditText) rootView.findViewById(R.id.editAddress);
        editAddress.setText(address);

        Button btnUpdateBasicInfo = (Button) rootView.findViewById(R.id.btnUpdateBasicInfo);
        btnUpdateBasicInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                formValidator.validate();
            }
        });

        Button btnReporterImage = (Button) rootView.findViewById(R.id.btnReporterImage);
        btnReporterImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getActivity(), v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_popup_reporter_image_actions, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.popup_Gallery:
                            browseImage();
                            return true;
                        case R.id.popupCamera:
                            takePhoto();
                            return true;
                        case R.id.popupReset:
                            new DownLoadImageUtility(imageReporter).execute(image);
                            imageFileName = "";
                            return true;
                        default:
                            return false;
                    }
                    }
                });
                popup.show();
            }
        });

        return rootView;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_RECORD_IMAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
                break;
            case REQUEST_SELECT_IMAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    if (pickPhoto.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivityForResult(pickPhoto, REQUEST_IMAGE_BROWSER);
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageReporter.setImageBitmap(ImageUtility.compressBitmap(imageBitmap, 50, true));

            // Save image from image view
            imageFileName = ImageUtility.saveImageToDisk(imageReporter, Report.TEMP_PATH, Report.TEMP_IMAGE_FILENAME);
            uploadImageToServer();
        } else if (requestCode == REQUEST_IMAGE_BROWSER && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            imageReporter.setImageURI(selectedImage);
            imageFileName = getRealPathFromURI(getContext(), selectedImage);
            uploadImageToServer();
        }
    }

    public void browseImage() {
        if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(browsePermissions, REQUEST_SELECT_IMAGE_PERMISSION);
        } else {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            if (pickPhoto.resolveActivity(getContext().getPackageManager()) != null) {
                startActivityForResult(pickPhoto, REQUEST_IMAGE_BROWSER);
            }
        }
    }

    public void takePhoto() {
        if (getActivity().checkSelfPermission(Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(cameraPermissions, REQUEST_RECORD_IMAGE_PERMISSION);
        } else {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    /**
     * Get real image absolute path from Uri
     */
    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void onDatePicker(View v) {
        String []arrayBirthDate = editBirthDate.getText().toString().split("-", 3);

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this.getContext(),
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                dateSetListener,
                Integer.parseInt(arrayBirthDate[2]),
                Integer.parseInt(arrayBirthDate[1]) - 1,
                Integer.parseInt(arrayBirthDate[0])
        );

        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        datePickerDialog.show();

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month += 1;
                String selectedDate = dayOfMonth > 9 ? "" + dayOfMonth : "0" + dayOfMonth;
                selectedDate += "-" + (month > 9 ? "" + month : "0" + month) + "-" + year;
                editBirthDate.setText(selectedDate);
            }
        };
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Get sex from RadioGroup
     */
    public String getSelectedSex() {
        String sex = "Other";
        switch (gradioSex.getCheckedRadioButtonId()) {
            case R.id.radioMale:
                sex = "Male";
                break;
            case R.id.radioFemale:
                sex = "Female";
                break;
        }

        return sex;
    }

    public void uploadImageToServer() {
        if (!imageFileName.equalsIgnoreCase("")) {
            File file = new File(imageFileName);
            RequestBody imageFilePart = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part bodyFile = MultipartBody.Part.createFormData("file", file.getName(), imageFilePart);

            /**
             * Pre-upload image to server then we will use only image string path to server for next request
             */
            FileClient fileClient = apiService.createClient(FileClient.class, APIService.getApiTokenForRequest());
            fileClient.uploadFile(bodyFile).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    int statusCode = response.code();
                    if (statusCode == HttpUtility.STATUS_CODE_OK) {
                        try {
                            String json = response.body().string();
                            JSONObject jsonObject = new JSONObject(json);
                            JSONObject responseData = jsonObject.getJSONObject("data");
                            imageFileName = responseData.getString("file");
                        } catch (JSONException e) {
                            Log.d(TAG, "onResponse JSONException: " + e.getMessage());
                        } catch (IOException e) {
                            Log.d(TAG, "onResponse IOException: " + e.getMessage());
                        }
                    } else {
                        Toast.makeText(getContext(), HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getContext(), HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public void onValidationSucceeded() {
        String sex = getSelectedSex();
        String birthDate = editBirthDate.getText().toString().trim();
        String dateToStore = adjustDateToStore(birthDate, "-").trim();
        String address = editAddress.getText().toString().trim();
        String firstName = editFirstName.getText().toString().trim();
        String lastName = editLastName.getText().toString().trim();

        String finalImageFileName = imageFileName.trim();
        if (imageFileName.equalsIgnoreCase(image)) {
            finalImageFileName = "";
        }

        ReporterClient reporterClient = apiService.createClient(ReporterClient.class, APIService.getAccessTokenForRequest(accessToken));
        reporterClient.updateInfo(
            reporterId,
            sex,
            dateToStore,
            address,
            finalImageFileName,
            firstName,
            lastName
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                if (statusCode == HttpUtility.STATUS_CODE_OK) {
                    try {
                        String json = response.body().string();
                        JSONObject jsonObject = new JSONObject(json);
                        JSONObject userInfo = jsonObject.getJSONObject("data");

                        SocialEyeDbHelper dbHelper = new SocialEyeDbHelper(getContext());
                        Reporter reporter = ReporterContract.getReporter(dbHelper.getReadableDatabase());
                        reporter.setSex(userInfo.getString("sex"));


                        String dateToStore = "";
                        String[] partsBirthDate = userInfo.getString("birth_date").split("\\.", 3);
                        if (partsBirthDate.length == 3) {
                            StringBuilder birthDateBuilder = new StringBuilder();
                            birthDateBuilder.append(partsBirthDate[2]).append("-").append(partsBirthDate[1]).append("-").append(partsBirthDate[0]);
                            dateToStore = birthDateBuilder.toString();
                        }
                        reporter.setBirthDate(dateToStore);

                        reporter.setAddress(userInfo.getString("address"));
                        reporter.setFirstName(userInfo.getString("first_name"));
                        reporter.setLastName(userInfo.getString("last_name"));

                        String image = userInfo.getString("image");
                        if (image.equalsIgnoreCase("")) {
                            image = null;
                        }
                        reporter.setImage(image);

                        // Update reporter from server to local Sqlite
                        long result = ReporterContract.update(dbHelper.getWritableDatabase(), reporter);

                        Toast.makeText(getContext(), "Basic information was saved successfully.", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        Log.d(TAG, "onResponse JSONException: " + e.getMessage());
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse IOException: " + e.getMessage());
                    }

                } else {
                    Toast.makeText(getContext(), "Cannot update basic information.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String adjustDateToStore(String date, String delimiter) {
        String[] parts = date.split(delimiter, 3);
        StringBuilder storeDate = new StringBuilder();
        storeDate.append(parts[2]).append(".").append(parts[1]).append(".").append(parts[0]);
        return storeDate.toString();
    }
}
