package nguonchhay.reasei.socialeyes.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import nguonchhay.reasei.socialeyes.LoginActivity;
import nguonchhay.reasei.socialeyes.R;
import nguonchhay.reasei.socialeyes.database.ReporterContract;
import nguonchhay.reasei.socialeyes.database.SocialEyeDbHelper;
import nguonchhay.reasei.socialeyes.model.Reporter;
import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.ReporterClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReporterCredentialFragment extends Fragment {

    private APIService apiService;
    private int reporterId;
    private String accessToken;

    private String email;
    private EditText editPassword;
    private EditText editConfirmPassword;

    public ReporterCredentialFragment() {}

    // newInstance constructor for creating fragment with arguments
    public static ReporterCredentialFragment newInstance(Reporter reporter) {
        ReporterCredentialFragment reporterCredentialFragment = new ReporterCredentialFragment();
        Bundle args = new Bundle();

        args.putString("email", reporter.getEmail());
        args.putString("accessToken", reporter.getRememberToken());
        args.putInt("reporterId", reporter.getId());

        reporterCredentialFragment.setArguments(args);
        return reporterCredentialFragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        email = getArguments().getString("email", "");
        accessToken = getArguments().getString("accessToken");
        reporterId = getArguments().getInt("reporterId");

        apiService = new APIService();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reporter_credential, container, false);
        EditText editEmail = (EditText) rootView.findViewById(R.id.editEmail);
        editEmail.setText(email);

        editPassword = (EditText) rootView.findViewById(R.id.editPassword);
        editConfirmPassword = (EditText) rootView.findViewById(R.id.editConfirmPassword);

        Button btnUpdateCredential = (Button) rootView.findViewById(R.id.btnUpdateCredential);
        btnUpdateCredential.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUpdateCredential(v);
            }
        });

        return rootView;
    }

    public void onUpdateCredential(View v) {
        String password = editPassword.getText().toString().trim();
        String confirmPassword = editConfirmPassword.getText().toString().trim();
        if (!password.equalsIgnoreCase("") && !confirmPassword.equalsIgnoreCase("")) {
            if (!password.equalsIgnoreCase(confirmPassword)) {
                Toast.makeText(this.getContext(), "Password and confirm password must be the same.", Toast.LENGTH_LONG).show();
            } else {
                ReporterClient reporterClient = apiService.createClient(ReporterClient.class, APIService.getAccessTokenForRequest(accessToken));
                reporterClient.updateCredential(
                    reporterId,
                    editPassword.getText().toString(),
                    editConfirmPassword.getText().toString()
                ).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        int statusCode = response.code();
                        if (statusCode == HttpUtility.STATUS_CODE_OK) {
                            SocialEyeDbHelper dbHelper = new SocialEyeDbHelper(getContext());
                            // Clean up reporter data from table
                            ReporterContract.cleanTable(dbHelper.getWritableDatabase());

                            // Force to re-login screen
                            Intent intent = new Intent(getContext(), LoginActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getContext(), "Cannot update credential.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getContext(), HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            Toast.makeText(getContext(), "Fill password and confirm password if you want to change.", Toast.LENGTH_SHORT).show();
        }
    }
}
