package nguonchhay.reasei.socialeyes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.LoginClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends AppCompatActivity implements ValidationListener {

    private Validator formValidator;
    private APIService apiService;

    private String email;

    @Length(min = 6, max = 6, message = "Verify code must be 6 digits")
    private EditText editVerifyCode;

    @Password(min = 8, scheme = Password.Scheme.ALPHA_NUMERIC_MIXED_CASE_SYMBOLS, message = "Password should contain alpha lowercase and upper case, number, and symbol")
    private EditText editPassword;

    @ConfirmPassword
    private EditText editConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_back);

        email = getIntent().getStringExtra("email");
        editVerifyCode = (EditText) findViewById(R.id.editVerifyCode);
        editPassword = (EditText) findViewById(R.id.editNewPassword);
        editConfirmPassword = (EditText) findViewById(R.id.editConfirmPassword);

        formValidator = new Validator(this);
        formValidator.setValidationListener(this);
        apiService = new APIService();

        Button btnResetPassoword = (Button) findViewById(R.id.btnResetPassword);
        btnResetPassoword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                formValidator.validate();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, ForgetPasswordActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackToLogin(View v) {
        Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onValidationSucceeded() {
        String verifyCode = editVerifyCode.getText().toString();
        String password = editPassword.getText().toString();

        LoginClient loginClient = apiService.createClientWithFormUrlEncoded(LoginClient.class, APIService.getApiTokenForRequest());
        loginClient.resetPassword(email, verifyCode, password).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                if (statusCode == HttpUtility.STATUS_CODE_OK) {
                    Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else if (statusCode == HttpUtility.STATUS_CODE_REQUIRED_FIELD) {
                    Toast.makeText(ResetPasswordActivity.this, "Fields are required", Toast.LENGTH_LONG).show();
                } else if (statusCode == HttpUtility.STATUS_CODE_UNPROCESSABLE_ENTITY) {
                    Toast.makeText(ResetPasswordActivity.this, "ReporterContract does not exist.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(ResetPasswordActivity.this, "Cannot make an request to server.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(ResetPasswordActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
