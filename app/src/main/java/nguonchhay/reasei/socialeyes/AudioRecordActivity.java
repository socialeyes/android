package nguonchhay.reasei.socialeyes;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import nguonchhay.reasei.socialeyes.model.Report;
import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.FileClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class AudioRecordActivity extends AppCompatActivity {

    private MediaRecorder mediaRecorder;
    private MediaPlayer mediaPlayer;
    private String outputFile;
    private boolean isStartRecording;
    private boolean isTakeMorePhoto;

    private ImageView imageRecord;
    private ImageView imagePlay;
    private ImageView imageDelete;
    private TextView textRecord;

    private String reportTypeId;
    private String reportAudioFileName;

    private String [] audioPermissions = {
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 1000;

    private APIService apiService = new APIService();

    private boolean alreadyRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_record);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_back);

        reportTypeId = getIntent().getStringExtra("reportTypeId");
        reportAudioFileName = "";

        imageRecord = (ImageView) findViewById(R.id.imageRecord);
        imageRecord.setImageResource(R.drawable.ic_action_audio_record);

        imagePlay = (ImageView) findViewById(R.id.imagePlay);
        imagePlay.setEnabled(false);

        imageDelete = (ImageView) findViewById(R.id.imageDelete);
        imageDelete.setEnabled(false);

        textRecord = (TextView) findViewById(R.id.textRecord);
        textRecord.setText("");

        outputFile = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/" + Report.TEMP_PATH + "/" + Report.TEMP_AUDIO_FILENAME;
        isStartRecording = true;
        alreadyRecord = false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isStartRecording = true;
                    startRecording();
                    break;
                }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, ReportActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onPlayAudio(View v) {
        startPlaying();
    }

    public void onDeleteAudio(View v) {
        isStartRecording = true;
        alreadyRecord = false;
        updateRecordStatus();
        textRecord.setText("");
        imageRecord.setImageResource(R.drawable.ic_action_audio_record);
    }

    public void onRecord(View v) {
        if (ContextCompat.checkSelfPermission(AudioRecordActivity.this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AudioRecordActivity.this, audioPermissions, REQUEST_RECORD_AUDIO_PERMISSION);
        } else {
            if (isStartRecording) {
                startRecording();
            } else {
                stopRecording();
            }
        }
    }

    public void onTakeMorePhoto(View v) {
        stopPlaying();
        isTakeMorePhoto = true;
        uploadAudioAndStartActivity();
    }

    public void onNext(View v) {
        if (alreadyRecord) {
            stopPlaying();
            isTakeMorePhoto = false;
            uploadAudioAndStartActivity();
        } else {
            Toast.makeText(this, "Please provide any audio record.", Toast.LENGTH_LONG).show();
        }
    }

    private void setUpMediaRecorder() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(outputFile);
    }

    private void startRecording() {
       setUpMediaRecorder();
        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
            updateRecordStatus();
            isStartRecording = false;
            alreadyRecord = true;
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateRecordStatus() {
        if (isStartRecording) {
            imageRecord.setImageResource(R.drawable.ic_action_audio_stop_record);
            imageRecord.setEnabled(true);
            textRecord.setText("Start recording...");
            imagePlay.setEnabled(false);
            imageDelete.setEnabled(false);
        } else {
            textRecord.setText("Finish recording");
            imagePlay.setEnabled(true);
            imageDelete.setEnabled(true);
            imageRecord.setEnabled(false);
        }
    }

    private void stopRecording() {
        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;
        updateRecordStatus();
    }

    private void startPlaying() {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(outputFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            Log.d(TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void uploadAudioAndStartActivity() {
        if (alreadyRecord) {
            File file = new File(outputFile);
            if (file.exists()) {
                RequestBody imageFilePart = RequestBody.create(MediaType.parse("audio/*"), file);
                MultipartBody.Part bodyFile = MultipartBody.Part.createFormData("file", file.getName(), imageFilePart);

                /**
                 * Pre-upload audio file to server then we will use only audio string path to server for next request
                 */
                FileClient fileClient = apiService.createClient(FileClient.class, APIService.getApiTokenForRequest());
                fileClient.uploadFile(bodyFile).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        int statusCode = response.code();
                        if (statusCode == HttpUtility.STATUS_CODE_OK) {
                            try {
                                String json = response.body().string();
                                JSONObject jsonObject = new JSONObject(json);
                                JSONObject responseData = new JSONObject(jsonObject.optString("data"));
                                reportAudioFileName = responseData.getString("file");

                                if (isTakeMorePhoto) {
                                    Intent intent = new Intent();
                                    intent.putExtra("savedAudioFileName", reportAudioFileName);
                                    setResult(ReportActivity.INTENT_AUDIO_RECORD_REQUEST_CODE, intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent(AudioRecordActivity.this, AuthorizeReportActivity.class);
                                    intent.putExtra("reportTypeId", reportTypeId);
                                    intent.putExtra("audioFileName", reportAudioFileName);
                                    intent.putExtra("imageFileName", "");
                                    intent.putExtra("description", "");
                                    startActivity(intent);
                                }

                            } catch (JSONException e) {
                                Log.d(TAG, "onResponse JSONException: " + e.getMessage());
                            } catch (IOException e) {
                                Log.d(TAG, "onResponse IOException: " + e.getMessage());
                            }
                        } else {
                            Toast.makeText(AudioRecordActivity.this, HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(AudioRecordActivity.this, HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                Toast.makeText(
                        AudioRecordActivity.this,
                        "Something went wrong. Please delete and try to record audio again",
                        Toast.LENGTH_LONG
                ).show();
            }
        } else {
            if (isTakeMorePhoto) {
                Intent intent = new Intent();
                intent.putExtra("savedAudioFileName", "");
                setResult(ReportActivity.INTENT_AUDIO_RECORD_REQUEST_CODE, intent);
                finish();
            }
        }
    }
}
