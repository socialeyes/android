package nguonchhay.reasei.socialeyes.utility;

public class HttpUtility {

    public static final String BASE_URL = "http://socialeye.reasei.co";
    public static final String API_TOKEN = "8f7484fdd17b6ea927e2fdfe99e34711716c04159b606f8c0f7626039a39ec7181a4395d9f59915676faf55f07687e0c59ea";

    public static final int STATUS_CODE_OK = 200;
    public static final int STATUS_CODE_NOT_MODIFIED = 304;
    public static final int STATUS_CODE_REQUIRED_FIELD = 400;
    public static final int STATUS_CODE_UNAUTHORIZED = 401;
    public static final int STATUS_CODE_NOT_FOUND = 404;
    public static final int STATUS_CODE_REQUEST_TIMEOUT = 408;
    public static final int STATUS_CODE_UNPROCESSABLE_ENTITY = 422;
    public static final int STATUS_CODE_SERVER_ERROR = 500;

    public static final String MESSAGE_REQUEST_ERROR = "Cannot make any request to server.";
    public static final String MESSAGE_PROCESS_DATA_ERROR = "Something went wrong after received data.";
    public static final String MESSAGE_SERVER_ERROR = "Something went wrong at server.";
}
