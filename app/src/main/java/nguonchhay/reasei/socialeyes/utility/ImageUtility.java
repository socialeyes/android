package nguonchhay.reasei.socialeyes.utility;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageUtility {

    /**
     * Compress Bitmap image
     */
    public static Bitmap compressBitmap(Bitmap original, int quality, boolean isPNG) {
        // Initialize a new ByteArrayStream
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        // Compress the bitmap with PNG format and quality
        if (isPNG) {
            original.compress(Bitmap.CompressFormat.PNG, quality, stream);
        } else {
            original.compress(Bitmap.CompressFormat.JPEG, quality, stream);
        }

        byte[] byteArray = stream.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray,0,byteArray.length);

        return compressedBitmap;
    }

    /**
     * Save image from ImageView
     */
    public static String saveImageToDisk(ImageView imageView, String storePath, String filename) {
        String pathAndFileName = "";
        //to get the image from the ImageView (say iv)
        BitmapDrawable draw = (BitmapDrawable) imageView.getDrawable();
        Bitmap bitmap = draw.getBitmap();

        FileOutputStream outStream = null;
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/" + storePath);
        dir.mkdirs();

        String fileName = filename + ".png";
        try {
            File outFile = new File(dir, fileName);
            outStream = new FileOutputStream(outFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);

            outStream.flush();
            outStream.close();

            pathAndFileName = outFile.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pathAndFileName;
    }
}
