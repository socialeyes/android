package nguonchhay.reasei.socialeyes;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nguonchhay.reasei.socialeyes.adapter.ReportDetailAdapter;
import nguonchhay.reasei.socialeyes.database.ReportContract;
import nguonchhay.reasei.socialeyes.database.ReportDetailContract;
import nguonchhay.reasei.socialeyes.database.SocialEyeDbHelper;
import nguonchhay.reasei.socialeyes.model.Report;
import nguonchhay.reasei.socialeyes.model.ReportDetail;
import nguonchhay.reasei.socialeyes.utility.DownLoadImageUtility;

import static android.content.ContentValues.TAG;

public class ReportListDetailActivity extends AppCompatActivity {

    private String audioUrl;
    private ImageView imagePlayAudio;
    private boolean isPlaying;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_list_detail);

        SocialEyeDbHelper dbHelper = new SocialEyeDbHelper(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_back);


        int reportId = Integer.parseInt(getIntent().getStringExtra("reportId"));
        Report report = ReportContract.getReport(dbHelper.getReadableDatabase(), reportId);


        ListView listReportDetail = (ListView) findViewById(R.id.listReportDetail);
        audioUrl = report.getAudio();
        ImageView image = (ImageView) findViewById(R.id.imageReportImage);
        new DownLoadImageUtility(image).execute(report.getImage());

        imagePlayAudio = (ImageView) findViewById(R.id.imagePlayAudio);
        isPlaying = false;


        TextView description = (TextView) findViewById(R.id.textReportDescription);
        description.setText(report.getDescription());

        if (report.getId() > 0) {
            List<ReportDetail> reportDetails = ReportDetailContract.getReportDetailsByReport(dbHelper.getReadableDatabase(), report.getId());

            List<String> reportDetailId = new ArrayList<>();
            List<String> reportDetailImage = new ArrayList<>();
            List<String> reportDetailName = new ArrayList<>();
            List<String> reportDetailStatus = new ArrayList<>();
            List<String> reportDetailContactPhone = new ArrayList<>();
            List<String> reportDetailEmail = new ArrayList<>();

            for (int i = 0; i < reportDetails.size(); i++) {
                ReportDetail reportDetail = (ReportDetail) reportDetails.get(i);
                try {
                    JSONObject objAuthorizeReport = new JSONObject(reportDetail.getAuthorizeReportJson());
                    JSONObject objAuthorizePlace = objAuthorizeReport.getJSONObject("authorize_place");

                    reportDetailId.add(objAuthorizePlace.getInt("id") + "");
                    reportDetailImage.add(objAuthorizePlace.getString("logo"));
                    reportDetailName.add(objAuthorizePlace.getString("name"));
                    reportDetailContactPhone.add(objAuthorizePlace.getString("contact_phone"));
                    reportDetailEmail.add(objAuthorizePlace.getString("email"));

                    JSONObject objStatus = new JSONObject(reportDetail.getReportStatusJson());
                    reportDetailStatus.add(objStatus.getString("name"));
                } catch (JSONException e) {
                    Log.d(TAG, "onResponse JSONException: " + e.getMessage());
                }
            }

            // Add data to ListView
            ReportDetailAdapter reportDetailAdapter = new ReportDetailAdapter(
                    ReportListDetailActivity.this,
                    reportDetailId,
                    reportDetailImage,
                    reportDetailName,
                    reportDetailStatus,
                    reportDetailContactPhone,
                    reportDetailEmail
            );
            listReportDetail.setAdapter(reportDetailAdapter);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, ReportListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onPlayAudio(View v) {
        if (!isPlaying) {
            isPlaying = true;
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlaying();
                }
            });

            try {
                mediaPlayer.setDataSource(audioUrl);
                mediaPlayer.prepare();
                mediaPlayer.start();
                imagePlayAudio.setImageResource(R.drawable.ic_action_pause);
            } catch (IOException e) {
                Log.e(TAG, "prepare() failed");
            }
        } else {
            stopPlaying();
        }
    }

    private void stopPlaying() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
            isPlaying = false;
            imagePlayAudio.setImageResource(R.drawable.ic_action_play);
        }
    }
}
