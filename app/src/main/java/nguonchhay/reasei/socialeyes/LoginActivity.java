package nguonchhay.reasei.socialeyes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Order;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import nguonchhay.reasei.socialeyes.database.ReporterContract;
import nguonchhay.reasei.socialeyes.database.SocialEyeDbHelper;
import nguonchhay.reasei.socialeyes.model.Reporter;
import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.LoginClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class LoginActivity extends Activity implements Validator.ValidationListener {

    private Validator formValidator;
    private APIService apiService;
    private SocialEyeDbHelper dbHelper;

    @Order(1)
    @Email
    private EditText editEmail;

    @Order(2)
    @Password(min = 8, message = "Please provide at least 8 digits")
    private EditText editPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        dbHelper = new SocialEyeDbHelper(this);

        editEmail = (EditText) findViewById(R.id.editEmail);
        editPassword = (EditText) findViewById(R.id.editPassword);

        formValidator = new Validator(this);
        formValidator.setValidationListener(this);
        apiService = new APIService();
    }

    /**
     * Handle sign in
     */
    public void onSignIn(View v) {
        formValidator.validate();
    }

    /**
     * Handle sign up link click
     */
    public void onSignUpClick(View v) {
        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(intent);
    }

    /**
     * Handle forget password link click
     */
    public void onForgetPasswordClick(View v) {
        Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
        startActivity(intent);
    }

    @Override
    public void onValidationSucceeded() {
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();

        LoginClient loginClient = apiService.createClient(LoginClient.class, APIService.getApiTokenForRequest());
        loginClient.login(email, password).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                if (statusCode == HttpUtility.STATUS_CODE_OK) {
                    try {
                        String json = response.body().string();
                        JSONObject jsonObject = new JSONObject(json);
                        JSONObject userInfo = jsonObject.getJSONObject("data");
                        int id = Integer.parseInt(userInfo.getString("id"));
                        String firstName = userInfo.getString("first_name");
                        String lastName = userInfo.getString("last_name");
                        String email = userInfo.getString("email");
                        String sex = userInfo.getString("sex");
                        String birthDate = userInfo.getString("birth_date");
                        String address = userInfo.getString("address");
                        String image = userInfo.getString("image");
                        if (image.equalsIgnoreCase("")) {
                            image = null;
                        }
                        int activation = Integer.parseInt(userInfo.getString("activate"));
                        String rememberToken = userInfo.getString("remember_token");
                        String startedSession = userInfo.getString("started_session");

                        Reporter reporter = new Reporter(
                            id, firstName, lastName, email, sex, birthDate, address, image, startedSession, activation,rememberToken
                        );

                        // Clean up reporter data from table
                        ReporterContract.cleanTable(dbHelper.getWritableDatabase());

                        // Store access_token to Sqlite for usage
                        long result = ReporterContract.insert(dbHelper.getWritableDatabase(), reporter);

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    } catch (JSONException e) {
                        Log.d(TAG, "onResponse JSONException: " + e.getMessage());
                    } catch (IOException e) {
                        Log.d(TAG, "onResponse IOException: " + e.getMessage());
                    }
                } else if (statusCode == HttpUtility.STATUS_CODE_REQUIRED_FIELD) {
                    Toast.makeText(LoginActivity.this, "Fields are required", Toast.LENGTH_LONG).show();
                } else if (statusCode == HttpUtility.STATUS_CODE_UNAUTHORIZED) {
                    Toast.makeText(LoginActivity.this, "Password is incorrect.", Toast.LENGTH_LONG).show();
                } else if (statusCode == HttpUtility.STATUS_CODE_NOT_FOUND) {
                    Toast.makeText(LoginActivity.this, "Email does not existing in the system.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Cannot make an request to server.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
