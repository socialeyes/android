package nguonchhay.reasei.socialeyes.model;

public class ReportDetail {

    private int id;
    private int reportId;
    private String  reportStatusJson;
    private String authorizeReportJson;
    private String responseDateTime;

    public ReportDetail() {}

    public ReportDetail(int id, int reportId, String reportStatusJson, String authorizeReportJson, String responseDateTime) {
        setId(id);
        setReportId(reportId);
        setReportStatusJson(reportStatusJson);
        setAuthorizeReportJson(authorizeReportJson);
        setResponseDateTime(responseDateTime);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReportId() {
        return reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    public String getReportStatusJson() {
        return reportStatusJson;
    }

    public void setReportStatusJson(String reportStatusJson) {
        this.reportStatusJson = reportStatusJson;
    }

    public String getAuthorizeReportJson() {
        return authorizeReportJson;
    }

    public void setAuthorizeReportJson(String authorizeReportJson) {
        this.authorizeReportJson = authorizeReportJson;
    }

    public String getResponseDateTime() {
        return responseDateTime;
    }

    public void setResponseDateTime(String responseDateTime) {
        this.responseDateTime = responseDateTime;
    }
}
