package nguonchhay.reasei.socialeyes.model;

public class ReportType {

    private int id;
    private String name;
    private String image;

    public ReportType(int id, String name, String image) {
        setId(id);
        setName(name);
        setImage(image);
    }

    /**
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     * @param int id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * @param String name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return String
     */
    public String getImage() {
        return image;
    }

    /**
     * @param String image
     */
    public void setImage(String image) {
        this.image = image;
    }
}
