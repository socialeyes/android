package nguonchhay.reasei.socialeyes.model;

public class Reporter {

    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String sex;
    private String birthDate;
    private String address;
    private String image;
    private int activation;
    private String rememberToken;
    private String startedSession;

    public Reporter(){}

    public Reporter(int id, String email, String sex, String birthDate, String address) {
        setId(id);
        setEmail(email);
        setSex(sex);
        setBirthDate(birthDate);
        setAddress(address);
    }

    public Reporter(int id, String firstName, String lastName, String email, String sex, String birthDate, String address, String image, String startedSession, int activation, String rememberToken) {
        setId(id);
        setFirstName(firstName);
        setLastName(lastName);
        setEmail(email);
        setSex(sex);
        setBirthDate(birthDate);
        setAddress(address);
        setImage(image);
        setActivation(activation);
        setStartedSession(startedSession);
        setRememberToken(rememberToken);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getDisplayBirthDate() {
        String []arrayDates = birthDate.split("-", 3);
        if (arrayDates.length == 3) {
            return arrayDates[2] + "-" + arrayDates[1] + "-" + arrayDates[0];
        }
        return "";
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getActivation() {
        return activation;
    }

    public void setActivation(int activation) {
        this.activation = activation;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getStartedSession() {
        return startedSession;
    }

    public void setStartedSession(String startedSession) {
        this.startedSession = startedSession;
    }
}
