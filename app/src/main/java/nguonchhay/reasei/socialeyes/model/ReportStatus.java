package nguonchhay.reasei.socialeyes.model;

import android.graphics.Color;

public class ReportStatus {
    public static final int COLOR_REPORTING = Color.parseColor("#ff9999");
    public static final int COLOR_TOOK_ACTION = Color.parseColor("#ffe6e6");
    public static final int COLOR_UNRESOLVED = Color.parseColor("#ffffe5");
}