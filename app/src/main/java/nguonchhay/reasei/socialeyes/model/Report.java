package nguonchhay.reasei.socialeyes.model;

public class Report {

    public static final String TEMP_PATH = "temp_report";
    public static final String TEMP_AUDIO_FILENAME = "temp_report_audio.mp3";
    public static final String TEMP_IMAGE_FILENAME = "temp_report_image";

    private int id;
    private String audio;
    private String image;
    private String description;

    public Report() {}
    public Report(int id, String audio, String image, String description) {
        setId(id);
        setAudio(audio);
        setImage(image);
        setDescription(description);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
