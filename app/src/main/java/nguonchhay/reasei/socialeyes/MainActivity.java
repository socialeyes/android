package nguonchhay.reasei.socialeyes;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nguonchhay.reasei.socialeyes.adapter.ReportTypeAdapter;
import nguonchhay.reasei.socialeyes.database.ReporterContract;
import nguonchhay.reasei.socialeyes.database.SocialEyeDbHelper;
import nguonchhay.reasei.socialeyes.model.Reporter;
import nguonchhay.reasei.socialeyes.model.ReportType;
import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.ReportTypeClient;
import nguonchhay.reasei.socialeyes.utility.DownLoadImageUtility;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity {

    private GridView gridViewReportType;
    private List<ReportType> listReportType = new ArrayList<ReportType>();
    private APIService apiService = new APIService();

    private SocialEyeDbHelper dbHelper;

    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new SocialEyeDbHelper(this);
        Reporter reporter = ReporterContract.getReporter(dbHelper.getReadableDatabase());

        mDrawerLayout = findViewById(R.id.drawer_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_menu);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
            new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {
                    Intent intent = null;
                    switch (menuItem.getItemId()) {
                        case R.id.nav_logout:
                            // set item as selected to persist highlight
                            menuItem.setChecked(true);
                            // Clean report table
                            ReporterContract.cleanTable(dbHelper.getWritableDatabase());

                            // Redirect to login screen
                            intent = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                            break;

                        case R.id.nav_report_list:
                            // set item as selected to persist highlight
                            menuItem.setChecked(true);
                            intent = new Intent(MainActivity.this, ReportListActivity.class);
                            startActivity(intent);
                            finish();
                            break;

                        case R.id.nav_account:
                            // set item as selected to persist highlight
                            menuItem.setChecked(true);
                            intent = new Intent(MainActivity.this, ReporterActivity.class);
                            startActivity(intent);
                            finish();
                            break;
                    }

                    // close drawer when item is tapped
                    //mDrawerLayout.closeDrawers();

                    return true;
                }
            });

        View navigationHeaderView = navigationView.getHeaderView(0);
        navigationHeaderView.setBackgroundColor(Color.parseColor("#d9d9d9"));

        ImageView navHeaderImage = (ImageView) navigationHeaderView.findViewById(R.id.navHeaderImage);
        if (reporter.getImage() == null) {
            navHeaderImage.setImageResource(R.drawable.avatar);
        } else {
            // Load reporter image to Nav Header Image
            new DownLoadImageUtility(navHeaderImage).execute(reporter.getImage());
        }

        TextView navHeaderTitle = (TextView) navigationHeaderView.findViewById(R.id.navHeaderTitle);
        navHeaderTitle.setText(reporter.getFullName());

        gridViewReportType = (GridView) findViewById(R.id.listReportType);
        gridViewReportType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ReportType reporterType = (ReportType) listReportType.get(position);
                Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                intent.putExtra("reportTypeId", reporterType.getId() + "");
                intent.putExtra("reporterTypeName", reporterType.getName());
                startActivity(intent);
            }
        });

        ReportTypeClient reportTypeClient = apiService.createClient(ReportTypeClient.class, APIService.getApiTokenForRequest());
        reportTypeClient.list()
            .enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    int statusCode = response.code();
                    if (statusCode == HttpUtility.STATUS_CODE_OK) {
                        try {
                            String json = response.body().string();
                            JSONObject jsonObject = new JSONObject(json);
                            JSONArray responseData = new JSONArray(jsonObject.optString("data"));

                            for (int i =0; i < responseData.length(); i++) {
                                JSONObject row = responseData.getJSONObject(i);
                                int id = row.getInt("id");
                                String name = row.getString("name");
                                String imageUrl = row.getString("image");
                                listReportType.add(new ReportType(id, name, imageUrl));
                            }
                            gridViewReportType.setAdapter(new ReportTypeAdapter(MainActivity.this, listReportType));

                        } catch (JSONException e) {
                            Log.d(TAG, "onResponse JSONException: " + e.getMessage());
                        } catch (IOException e) {
                            Log.d(TAG, "onResponse IOException: " + e.getMessage());
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Something went wrong while trying to load Report Type", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(MainActivity.this, "Cannot make an request to server.", Toast.LENGTH_SHORT).show();
                }
            });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
