package nguonchhay.reasei.socialeyes;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import java.util.List;

import nguonchhay.reasei.socialeyes.database.ReporterContract;
import nguonchhay.reasei.socialeyes.database.SocialEyeDbHelper;
import nguonchhay.reasei.socialeyes.model.Reporter;
import nguonchhay.reasei.socialeyes.service.APIService;
import nguonchhay.reasei.socialeyes.service.ReportClient;
import nguonchhay.reasei.socialeyes.utility.HttpUtility;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportDeleteActivity extends AppCompatActivity implements Validator.ValidationListener {

    private APIService apiService;
    private Validator formValidator;

    private int reportId;
    private Reporter reporter;

    @NotEmpty(message = "Description is required.")
    private EditText editDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_delete);

        SocialEyeDbHelper dbHelper = new SocialEyeDbHelper(this);

        // Get logged in reporter
        reporter = ReporterContract.getReporter(dbHelper.getReadableDatabase());
        if (reporter.getRememberToken() == null) {
            Intent intentLogin = new Intent(ReportDeleteActivity.this, LoginActivity.class);
            startActivity(intentLogin);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_back);

        formValidator = new Validator(this);
        formValidator.setValidationListener(this);
        apiService = new APIService();


        reportId = Integer.parseInt(getIntent().getStringExtra("reportId"));
        editDescription = (EditText) findViewById(R.id.editDescription);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, ReportListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onDeleteReport(View v) {
        formValidator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        /**
         * Soft delete report from server
         */
        ReportClient reportClient = apiService.createClient(ReportClient.class, APIService.getAccessTokenForRequest(reporter.getRememberToken()));
        reportClient.delete(reporter.getId(), reportId, editDescription.getText().toString())
            .enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    int statusCode = response.code();
                    if (statusCode == HttpUtility.STATUS_CODE_OK) {
                        Intent intent = new Intent(ReportDeleteActivity.this, ReportListActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(ReportDeleteActivity.this, HttpUtility.MESSAGE_PROCESS_DATA_ERROR, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(ReportDeleteActivity.this, HttpUtility.MESSAGE_REQUEST_ERROR, Toast.LENGTH_LONG).show();
                }
            });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
