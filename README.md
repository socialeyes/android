Welcome to SocialEye Android Application
========================================

An Android mobile application to report all kinds of activities that happen in society such as accident, violence, traffic jam, robbery and so to the authorize people to handle that situation.

__Note__: this project is to complete Fundamental of Mobile Operatiing System course at Poland.

Maintainers
===========

* [Nguonchhay Touch](tnguonchhay@gmail.com)

Requirement
===========

1. Android Studio >= 3.1
2. Java >= 1.8

Local set up
============

1. Clone project from GitLab
```
git clone https://gitlab.com/socialeyes/android.git SocialEyesAndroid
```

2. Configure `local.properties` file
```bash
# Location of the SDK. This is only used by Gradle.
# For customization when using a Version Control System, please read the
# header note.
#Thu May 10 19:59:36 CEST 2018
ndk.dir=/Path/To/Android/sdk/ndk-bundle
sdk.dir=/Path/To/Android/sdk

# Configure variable by stage: local, staging, live
API_TOKEN="8f7484fdd17b6ea927e2fdfe99e34711716c04159b606f8c0f7626039a39ec7181a4395d9f59915676faf55f07687e0c59ea"
BASE_URL="http://socialeye.reasei.co"
REPORT_LIST_LIMIT=6
REPORT_LIST_NUM_PAGINATE=1
```

3. Open project with Android Studio